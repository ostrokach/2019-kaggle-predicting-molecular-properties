
https://github.com/jensengroup/xyz2mol/blob/master/xyz2mol.py


***Node features:***

- (Partial) charge.
- Moment.
- Number of hydrogens / number of bonds.

***Edge features:***

- Covalent bond (true / false).
- Bond order.
