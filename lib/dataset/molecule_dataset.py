import shutil
from pathlib import Path
from urllib.request import urlopen

import numpy as np
import tqdm

import pyarrow.parquet as pq
import torch
from torch_geometric.data import Data, InMemoryDataset

TYPE_INDEX = {
    "1JHC": 0,
    "2JHH": 1,
    "1JHN": 2,
    "2JHN": 3,
    "2JHC": 4,
    "3JHH": 5,
    "3JHC": 6,
    "3JHN": 7,
}
ATOM_INDEX = {"C": 0, "N": 1, "H": 2, "O": 3, "F": 4}


class MoleculeDataset(InMemoryDataset):

    raw_urls = {
        "final_molecules.parquet": "https://storage.googleapis.com/deep-molecule-gen/subdivide-dataset/final_molecules.parquet",  # noqa
        "final_atoms.parquet": "https://storage.googleapis.com/deep-molecule-gen/subdivide-dataset/final_atoms.parquet",  # noqa
        "final_train.parquet": "https://storage.googleapis.com/deep-molecule-gen/subdivide-dataset/final_train.parquet",  # noqa
        "final_valid.parquet": "https://storage.googleapis.com/deep-molecule-gen/subdivide-dataset/final_valid.parquet",  # noqa
        "final_test.parquet": "https://storage.googleapis.com/deep-molecule-gen/subdivide-dataset/final_test.parquet",  # noqa
    }

    edge_attributes = (
        ["rmsd", "is_aromatic", "is_covalent", "shortest_path_length"]
        + [f"bond_type_{i}" for i in range(5)]
        + [f"bond_index_{i}" for i in range(len(TYPE_INDEX) + 1)]
    )

    node_attributes = (
        [f"is_{i}" for i in ["C", "N", "H", "O", "F"]]
        + ["degree", "explicit_valence", "hybridization", "in_ring"]
        + ["mm"]
    )

    graph_attributes = [
        "rc_A",
        "rc_B",
        "rc_C",
        "mu",
        "alpha",
        "homo",
        "lumo",
        "gap",
        "r2",
        "zpve",
        "U0",
        "U",
        "H",
        "G",
        "Cv",
        "freqs_min",
        "freqs_max",
        "freqs_mean",
        "linear",
        "mulliken_min",
        "mulliken_max",
        "mulliken_mean",
    ]

    def __init__(
        self, root, subset, transform=None, pre_transform=None, pre_filter=None
    ) -> None:
        self.subset = subset
        self._raw_file_names = [
            "final_molecules.parquet",
            "final_atoms.parquet",
            f"final_{subset}.parquet",
        ]
        self._processed_file_names = [f"molecules_{subset}.pt"]
        self.edge_targets = (
            ["id", "scalar_coupling_constant"] if subset not in ["test"] else ["id"]
        )
        super().__init__(root, transform, pre_transform, pre_filter)
        self.data, self.slices = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return self._raw_file_names

    @property
    def processed_file_names(self):
        return self._processed_file_names

    def download(self):
        for file_name in self._raw_file_names:
            download_url(self.raw_urls[file_name], self.raw_dir)

    def process(self):
        data_df = pq.read_pandas(
            Path(self.raw_dir) / f"final_{self.subset}.parquet"
        ).to_pandas()
        atoms_df = pq.read_pandas(
            Path(self.raw_dir) / "final_atoms.parquet"
        ).to_pandas()
        molecules_df = pq.read_pandas(
            Path(self.raw_dir) / "final_molecules.parquet"
        ).to_pandas()

        molecules = set(data_df["molecule_name"])
        atoms_df = atoms_df[atoms_df["molecule_name"].isin(molecules)]
        molecules_df = molecules_df[molecules_df["molecule_name"].isin(molecules)]

        data_gp = sorted(data_df.groupby("molecule_name"), key=lambda x: x[0])
        atoms_gp = sorted(atoms_df.groupby("molecule_name"), key=lambda x: x[0])
        molecules_gp = sorted(molecules_df.groupby("molecule_name"), key=lambda x: x[0])

        data_list = []
        for (
            (data_molecule_name, data_gp),
            (atoms_molecule_name, atoms_gp),
            (molecules_molecule_name, molecules_gp),
        ) in tqdm.tqdm_notebook(
            zip(data_gp, atoms_gp, molecules_gp), total=len(molecules)
        ):
            assert data_molecule_name == atoms_molecule_name == molecules_molecule_name
            gp = data_gp.copy()
            edge_index = extract_edge_indices(gp)
            edge_attr = extract_edge_values(gp, self.edge_attributes)
            nodes = extract_nodes(gp, atoms_gp, self.node_attributes)
            targets = extract_edge_targets(gp, self.edge_targets)
            graph_attr = extract_molecule_features(molecules_gp, self.graph_attributes)
            data = Data(
                x=nodes,
                edge_index=edge_index,
                edge_attr=edge_attr,
                edge_attr_unk=targets,
                graph_attr=graph_attr,
            )
            data_list.append(data)
        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])

    def get(self, *args, **kwargs):
        data = super().get(*args, **kwargs)
        data.graph_attr = data.graph_attr.t().expand(
            (data.edge_attr.size(0), data.graph_attr.size(0))
        )
        return data


def download_url(url, folder):
    url = str(url)
    folder = str(folder)
    filename = url.rsplit("/", 1)[-1]
    folder = Path(folder)
    folder.mkdir(exist_ok=True)

    if url.startswith("file://") or url.startswith("/"):
        shutil.copy(url.replace("file://", ""), folder)
    else:
        chunk_size = 16 * 1024
        response = urlopen(url)
        with (folder / filename).open("wb") as f:
            while True:
                chunk = response.read(chunk_size)
                if not chunk:
                    break
                f.write(chunk)


def extract_edge_indices(gp):
    assert gp["atom_index_0"].notnull().all()
    assert gp["atom_index_1"].notnull().all()
    row = torch.from_numpy(gp["atom_index_0"].values).to(torch.long)
    col = torch.from_numpy(gp["atom_index_1"].values).to(torch.long)
    indices = torch.stack([row, col])
    return indices


def to_onehot(values, num_categories):
    out = torch.zeros((len(values), num_categories), dtype=torch.float32)
    out.scatter_(
        1,
        torch.from_numpy(values).reshape(-1, 1).to(torch.long),
        torch.ones((len(values), num_categories)),
    )
    assert (out.sum(dim=1) == 1).all().item()
    return out


def extract_edge_values(gp, columns):
    assert gp["rmsd"].notnull().all()
    assert gp["shortest_path_length"].notnull().all()

    gp["is_aromatic"] = gp["is_aromatic"].fillna(False)
    gp["is_covalent"] = gp["is_covalent"].fillna(False)
    values = torch.from_numpy(
        gp[[c for c in columns if not c.rsplit("_", 1)[-1].isdigit()]]
        .astype(np.float32)
        .values
    )
    bond_types = to_onehot(
        gp["bond_type"].map({1: 0, 2: 1, 3: 2, 12: 3}).fillna(4).values, 5
    )
    coupling_types = to_onehot(
        gp["type"].map(TYPE_INDEX).fillna(len(TYPE_INDEX)).values, len(TYPE_INDEX) + 1
    )
    values = torch.cat([values, bond_types, coupling_types], 1)
    assert values.size(1) == len(columns)
    return values


def extract_edge_targets(gp, columns):
    targets = torch.from_numpy(gp[columns].astype(np.float32).values)
    return targets


def extract_nodes(gp, final_atom_df, columns):
    atoms = (
        gp[["atom_id_0", "atom_index_0", "atom_0"]]
        .rename(
            columns={
                "atom_id_0": "atom_id",
                "atom_index_0": "atom_index",
                "atom_0": "atom",
            }
        )
        .drop_duplicates()
        .sort_values("atom_index")
    )
    len_before = len(atoms)
    atoms = atoms.merge(
        final_atom_df.drop("atom_index", axis=1), on=["atom", "atom_id"], how="left"
    )
    assert len(atoms) == len_before
    assert (atoms["atom_index"] == list(range(len(atoms)))).all(), atoms["atom_index"]

    atoms["is_C"] = (atoms["atom"] == "C").astype(np.float32)
    atoms["is_N"] = (atoms["atom"] == "N").astype(np.float32)
    atoms["is_H"] = (atoms["atom"] == "H").astype(np.float32)
    atoms["is_O"] = (atoms["atom"] == "O").astype(np.float32)
    atoms["is_F"] = (atoms["atom"] == "F").astype(np.float32)
    assert (
        atoms[[f"is_{i}" for i in ["C", "N", "H", "O", "F"]]].sum(axis=1) == 1
    ).all()
    nodes = torch.from_numpy(atoms[columns].values.astype(np.float32))
    return nodes


def extract_molecule_features(gp, columns):
    means = {
        "rc_A": 9.966178637507168,
        "rc_B": 1.4062204603624546,
        "rc_C": 1.127005936103231,
        "mu": 2.672955021984324,
        "alpha": 75.28793668514625,
        "homo": -0.2401871833301472,
        "lumo": 0.0118807088510801,
        "gap": 0.25206775148155225,
        "r2": 1189.5166253404702,
        "zpve": 0.1491291079640604,
        "U0": -410.8042303992812,
        "U": -410.7957584219613,
        "H": -410.79481423546554,
        "G": -410.83763544398397,
        "Cv": 31.624101976677498,
        "freqs_min": 83.24809217663925,
        "freqs_max": 3504.59442723074,
        "freqs_mean": 1350.5756191152611,
        "linear": 0.9999693347950415,
        "mulliken_min": -0.4386137306442363,
        "mulliken_max": 0.3400643650009559,
        "mulliken_mean": -1.0516888990094149e-10,
    }
    stds = {
        "rc_A": 1830.8546892014847,
        "rc_B": 1.597451567023777,
        "rc_C": 1.1036707062879716,
        "mu": 1.5004621918958434,
        "alpha": 8.147854315085896,
        "homo": 0.021890936090921735,
        "lumo": 0.04679203509408144,
        "gap": 0.047152154331155,
        "r2": 280.3719683816618,
        "zpve": 0.03307865053503071,
        "U0": 39.84134942805435,
        "U": 39.8411322145381,
        "H": 39.84113221396646,
        "G": 39.84185069898485,
        "Cv": 4.0626758716334725,
        "freqs_min": 49.61179833896082,
        "freqs_max": 265.5912256395537,
        "freqs_mean": 68.44129264604031,
        "linear": 0.00553755358322251,
        "mulliken_min": 0.0756523294987262,
        "mulliken_max": 0.09048787973333434,
        "mulliken_mean": 6.909125497810567e-08,
    }

    assert len(gp) == 1
    data = gp.iloc[0].to_dict()
    data = {k: (v - means[k]) / stds[k] for k, v in data.items() if k in columns}
    molecule_properties = torch.tensor([data[c] for c in columns], dtype=torch.float32)
    return molecule_properties.reshape(1, -1)
