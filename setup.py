from setuptools import find_packages, setup


def read_md(file):
    with open(file) as fin:
        return fin.read()


setup(
    name="lib",
    version="0.1.0",
    description="Auxilary files for datapkg.",
    long_description=read_md("README.md"),
    author="Alexey Strokach",
    author_email="alex.strokach@utoronto.ca",
    url="https://gitlab.com/ostrokach/2019-kaggle-predicting-molecular-properties",
    packages=find_packages(exclude=["input", "notebooks", "tests"]),
    package_data={},
    include_package_data=True,
    zip_safe=False,
    keywords="kaggle,predicting-molecular-properties",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
    ],
    test_suite="tests",
)
